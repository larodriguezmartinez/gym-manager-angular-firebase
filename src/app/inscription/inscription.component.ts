import { Component, OnInit } from '@angular/core';
import { Inscription } from '../models/inscription';
import { Client } from '../models/client';
import { AngularFirestore } from '@angular/fire/firestore';
import { Subscription } from '../models/subscription';
import { AlertsService } from '../services/alerts.service';

@Component({
  selector: 'app-inscription',
  templateUrl: './inscription.component.html',
  styleUrls: ['./inscription.component.scss']
})
export class InscriptionComponent implements OnInit {

  inscription: Inscription = new Inscription();

  clientSelect: Client = new Client();

  subscriptionSelect: Subscription = new Subscription();

  subscriptions: Subscription[] = new Array<Subscription>();

  idSubcription: number;

  constructor(private db: AngularFirestore, private alert: AlertsService) { }

  ngOnInit() {

    this.db.collection('subscriptions').get().subscribe((result)=>{

      result.docs.forEach((item) =>{

        let subscription = item.data() as Subscription;
        subscription.id = item.id;
        subscription.ref = item.ref;

        this.subscriptions.push(subscription);
      });
      // console.log(this.subscriptions)

    });

  }

  setClient(client: Client){

    this.inscription.client = client.ref;

    this.clientSelect = client;

  }

  clearClient(){

    this.clientSelect = new Client();

    this.inscription.client = undefined;

  }

  save(){

    if(this.inscription.validate().valid){

      let inscriptiontoAdd = {
        date: this.inscription.date,
        endPeriod: this.inscription.endPeriod,
        price: this.inscription.price,
        client: this.inscription.client,
        subtotal: this.inscription.subtotal,
        iva: this.inscription.iva,
        total: this.inscription.total,
      }

      this.db.collection('inscriptions').add(inscriptiontoAdd).then((result)=>{
        this.inscription = new Inscription;
        this.clientSelect = new Client;
        this.subscriptionSelect = new Subscription();
        this.idSubcription = null;
        this.alert.successAlert('Inscriptions','added successfully');

      })

    }else{

      this.alert.warningAlert('Inscriptions',this.inscription.validate().message);

    }

  }

  selectSubscription(id: string){

    if(id != "null" ){
      this.subscriptionSelect = this.subscriptions.find(x=> x.id == id)

      this.inscription.price = this.subscriptionSelect.ref;

      this.inscription.date = new Date();

      this.inscription.subtotal = this.subscriptionSelect.price;

      this.inscription.iva =     this.inscription.subtotal * 0.21;

      this.inscription.total = this.inscription.subtotal + this.inscription.iva ;

      if(this.subscriptionSelect.period == 1){
        let days: number = this.subscriptionSelect.duration;
        let endDate = new Date(this.inscription.date.getFullYear(),this.inscription.date.getMonth(),this.inscription.date.getDate() + days);
        this.inscription.endPeriod = endDate;
      }

      if(this.subscriptionSelect.period == 2){
        let days: number = this.subscriptionSelect.duration * 7;
        let endDate = new Date(this.inscription.date.getFullYear(),this.inscription.date.getMonth(),this.inscription.date.getDate() + days);
        this.inscription.endPeriod = endDate;
      }

      if(this.subscriptionSelect.period == 3){
        let days: number = this.subscriptionSelect.duration * 15;
        let endDate = new Date(this.inscription.date.getFullYear(),this.inscription.date.getMonth(),this.inscription.date.getDate() + days);
        this.inscription.endPeriod = endDate;
      }

      if(this.subscriptionSelect.period == 4){
        let duration = this.subscriptionSelect.duration
        let endDate = new Date(this.inscription.date.getFullYear(),this.inscription.date.getMonth()+duration,this.inscription.date.getDate());
        this.inscription.endPeriod = endDate;
      }

      if(this.subscriptionSelect.period == 5){

        let duration = this.subscriptionSelect.duration
        let endDate = new Date(this.inscription.date.getFullYear()+duration,this.inscription.date.getMonth(),this.inscription.date.getDate());
        this.inscription.endPeriod = endDate;
      }
    }else{

      this.inscription.endPeriod = null;

      this.inscription.date = null;

      this.subscriptionSelect = new Subscription();

      this.inscription.price = null;

      this.inscription.subtotal = 0;

      this.inscription.iva = 0;

      this.inscription.total = 0;
    }

  }
}
