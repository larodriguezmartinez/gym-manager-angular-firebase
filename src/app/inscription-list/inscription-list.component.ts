import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Inscription } from '../models/inscription';

@Component({
  selector: 'app-inscription-list',
  templateUrl: './inscription-list.component.html',
  styleUrls: ['./inscription-list.component.scss']
})
export class InscriptionListComponent implements OnInit {

  inscriptions: any[] = [];

  constructor(private db: AngularFirestore) { }

  ngOnInit() {

    this.inscriptions.length = 0;

    this.db.collection('inscriptions').get().subscribe((result)=>{

      result.docs.forEach((item) => {

        let inscription = item.data();
        inscription.id = item.id;


        this.db.doc(item.data().client.path).get().subscribe((result)=>{
          inscription.client = result.data()
        });

        this.inscriptions.push(inscription);

      });

    });
  }

}
