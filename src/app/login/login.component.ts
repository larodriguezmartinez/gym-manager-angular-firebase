import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AngularFireAuth } from '@angular/fire/auth';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  correctData: boolean = true;
  errorText: string;

  constructor( private createForm: FormBuilder, private afAuth: AngularFireAuth, private spinner: NgxSpinnerService ) {

    }

  ngOnInit() {

    this.loginForm = this.createForm.group({

      email: ['', Validators.compose([
        Validators.required, Validators.email
      ])],
      password: ['', Validators.required]
    })

  }

  login() {

    if(this.loginForm.valid){

      this.correctData = true;
      this.spinner.show();

      this.afAuth.auth.signInWithEmailAndPassword(this.loginForm.value.email, this.loginForm.value.password).then( (usuario) =>{
        // console.log(usuario)
        this.spinner.hide();
      }).catch((error)=>{
        // console.log(error)
        this.spinner.hide();
        this.correctData = false;
        this.errorText = error.message;

      });

    }else{

      this.correctData = false;
      this.errorText = "The password is invalid or the user does not have a password.";


    }
  }
}
