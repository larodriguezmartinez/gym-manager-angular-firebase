import { Component } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { User } from 'firebase';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  title = 'Gym-Manager';

  user: User;

  loading: boolean = true;

  constructor(private afAuth: AngularFireAuth, private spinner: NgxSpinnerService )  {

    this.spinner.show();

    this.afAuth.user.subscribe( (user) =>{

      setTimeout(() => {

        this.spinner.hide();

        this.user = user;

        this.loading = false;

      }, 1000);

    })

  }

  logout() {

    this.afAuth.auth.signOut();

  }

}
