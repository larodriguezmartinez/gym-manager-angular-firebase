import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AccordionModule } from 'ngx-bootstrap/accordion';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AngularFireModule } from '@angular/fire'
import { environment } from 'src/environments/environment';
import { AngularFireAuth } from '@angular/fire/auth';
import { LoginComponent } from './login/login.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { NgxSpinnerModule } from "ngx-spinner";
import { HeaderComponent } from './header/header.component';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { CollapseModule } from 'ngx-bootstrap/collapse';
import { ClientsListComponent } from './clients-list/clients-list.component';
import { AngularFirestore } from '@angular/fire/firestore';
import { AddClientComponent } from './add-client/add-client.component';
import { ProgressbarModule } from 'ngx-bootstrap/progressbar';
import { AngularFireStorageModule } from '@angular/fire/storage';
import { AlertsService } from './services/alerts.service';
import { SubscriptionsComponent } from './subscriptions/subscriptions.component';
import { InscriptionComponent } from './inscription/inscription.component';
import { SelectClientComponent } from './select-client/select-client.component';
import { InscriptionListComponent } from './inscription-list/inscription-list.component';


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HeaderComponent,
    ClientsListComponent,
    AddClientComponent,
    SubscriptionsComponent,
    InscriptionComponent,
    SelectClientComponent,
    InscriptionListComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    AccordionModule.forRoot(),
    BsDropdownModule.forRoot(),
    CollapseModule.forRoot(),
    ProgressbarModule.forRoot(),
    BrowserAnimationsModule,
    FormsModule,
    AngularFireModule.initializeApp(environment.firebase),
    ReactiveFormsModule,
    NgxSpinnerModule,
    AngularFireStorageModule

    ],
  providers: [
    AngularFireAuth,
    AngularFirestore,
    AlertsService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
