import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ClientsListComponent } from './clients-list/clients-list.component';
import { AddClientComponent } from './add-client/add-client.component';
import { SubscriptionsComponent } from './subscriptions/subscriptions.component';
import { InscriptionComponent } from './inscription/inscription.component';
import { InscriptionListComponent } from './inscription-list/inscription-list.component';


const routes: Routes = [
  { path: '', redirectTo: 'inscription' , pathMatch: 'full' },
  { path: 'inscription', component: InscriptionComponent },
  { path: 'clients', component: ClientsListComponent },
  { path: 'add-client', component: AddClientComponent },
  { path: 'add-client/:clientId', component: AddClientComponent },
  { path: 'subscriptions', component: SubscriptionsComponent },
  { path: 'inscription-list', component: InscriptionListComponent },


];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
