import { Component, OnInit } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AngularFireStorage } from '@angular/fire/storage';
import { AngularFirestore } from '@angular/fire/firestore';
import { ActivatedRoute } from '@angular/router';
import Swal from 'sweetalert2'
import { AlertsService } from '../services/alerts.service';

@Component({
  selector: 'app-add-client',
  templateUrl: './add-client.component.html',
  styleUrls: ['./add-client.component.scss']
})
export class AddClientComponent implements OnInit {

  formAddClient: FormGroup;
  percentUpload: number = 0;
  imageUrl: string = '';
  edit: boolean = false;
  clientId: string;

  constructor(
    private spinner: NgxSpinnerService,
    private form: FormBuilder,
    private storage: AngularFireStorage,
    private db: AngularFirestore,
    private activeRoute: ActivatedRoute,
    private alert: AlertsService ) { }

  ngOnInit() {

    this.spinner.show();

    this.formAddClient = this.form.group({

        name: ['', Validators.required],
        surname: ['', Validators.required],
        email: ['', Validators.compose([
          Validators.required,
          Validators.email
        ])],
        nif: ['', Validators.required],
        birthdate: ['', Validators.required],
        phone: ['', Validators.required],
        profileImgUrl: ['', Validators.required],

    });

    // console.log(this.activeRoute.snapshot.params.clientId);

    this.clientId = this.activeRoute.snapshot.params.clientId;

    if(this.clientId != undefined){

      this.edit = true;

      this.db.doc<any>('clients/'+this.clientId).valueChanges().subscribe((client) => {
        // console.log(client);

        this.formAddClient.setValue({

          name: client.name,
          surname: client.surname,
          email: client.email,
          phone: client.phone,
          birthdate:  new Date(client.birthdate.seconds * 1000).toISOString().substr(0,10),
          nif: client.nif,
          profileImgUrl: ''

        });

        this.imageUrl = client.profileImgUrl

        this.spinner.hide();
      })
    }

    this.spinner.hide();
  }

  addClient(){

    // console.log(this.formAddClient.value)

    this.formAddClient.value.profileImgUrl = this.imageUrl;

    this.formAddClient.value.birthdate = new Date(this.formAddClient.value.birthdate);

    this.db.collection('clients').add(this.formAddClient.value).then((finish) => {
      // console.log('finish')
      this.alert.successAlert('User','successfully added');

      this.formAddClient.reset();

    })

  }

  editClient(){

    this.formAddClient.value.profileImgUrl = this.imageUrl;

    this.formAddClient.value.birthdate = new Date(this.formAddClient.value.birthdate);

    this.db.doc('clients/'+this.clientId).update(this.formAddClient.value).then((finish) => {
      // console.log('finish')
      this.alert.successAlert('User','successfully edited');

      this.formAddClient.reset();

    }).catch(()=>{

      this.alert.errorAlert('User','an error occurred while updating');

    })

  }

  uploadImg(event){

    if(event.target.files.length > 0 ){

      let fileName = new Date().getTime().toString();
      let file = event.target.files[0];
      let extension = file.name.toString().substring(file.name.toString().lastIndexOf('.'));
      let filePath = 'clients/'+fileName+extension;
      let ref = this.storage.ref(filePath);
      let task = ref.put(file);

      task.then((result)=>{
        // console.log('img upload')
        ref.getDownloadURL().subscribe((url)=>{
          // console.log(url)
          this.imageUrl = url;
        })

      })

      task.percentageChanges().subscribe((percent)=>{
        this.percentUpload = parseInt(percent.toString());
      })

    }

  }

}
