import { Injectable } from '@angular/core';
import Swal from 'sweetalert2';

@Injectable({
  providedIn: 'root'
})
export class AlertsService {

  constructor() { }

  errorAlert( title:string, text:string){

    Swal.fire({
      title: title,
      text: text,
      icon: 'error',
    })

  }

  successAlert( title:string, text:string){

    Swal.fire({
      title: title,
      text: text,
      icon: 'success',
    })

  }

  warningAlert( title:string, text:string){

    Swal.fire({
      title: title,
      text: text,
      icon: 'warning',
    })

  }

}
