import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { NgxSpinnerService } from 'ngx-spinner';
import { Client } from '../models/client';

@Component({
  selector: 'app-clients-list',
  templateUrl: './clients-list.component.html',
  styleUrls: ['./clients-list.component.scss']
})
export class ClientsListComponent implements OnInit {

  clients: any[] = new Array<any>();

  constructor( private db: AngularFirestore, private spinner: NgxSpinnerService  ) { }

  ngOnInit() {

    this.spinner.show();

    this.clients.length = 0;

    this.db.collection('clients').get().subscribe((result) => {

      result.docs.forEach((item) =>{

        let client = item.data();
        client.id = item.id;
        client.ref = item.ref;
        client.visible = true;
        this.clients.push(client);

      });

      this.spinner.hide();

    });
  }


  searchClient(name: string){
    console.log(name)
    this.clients.forEach((client) => {
      if(client.name.toLocaleLowerCase().includes(name.toLocaleLowerCase()) || client.nif.toLocaleLowerCase().includes(name.toLocaleLowerCase())  || client.surname.toLocaleLowerCase().includes(name.toLocaleLowerCase())){
        client.visible = true;
      }else{
        client.visible = false;
      }
    })

  }

}
