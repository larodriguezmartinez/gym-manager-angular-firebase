import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AngularFirestore } from '@angular/fire/firestore';
import { AlertsService } from '../services/alerts.service';
import { Subscription } from '../models/subscription';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-subscriptions',
  templateUrl: './subscriptions.component.html',
  styleUrls: ['./subscriptions.component.scss']
})
export class SubscriptionsComponent implements OnInit {

  formSubscription: FormGroup;

  subscriptions: any[] = new Array<Subscription>();

  isEdit: boolean = false;

  id: string = '';

  constructor( private form: FormBuilder,
    private db: AngularFirestore,
    private alert: AlertsService) { }

  ngOnInit() {

    this.formSubscription = this.form.group({

      name : ['', Validators.required],
      price: ['', Validators.required],
      duration: ['', Validators.required],
      period: ['', Validators.required]

    })

    this.showSubscriptions();

  }

  showSubscriptions(){

    this.db.collection<Subscription>('subscriptions').get().subscribe((result)=>{{

      this.subscriptions.length = 0;

      result.docs.forEach((item) =>{

        let subscription = item.data() as Subscription;
        subscription.id = item.id;
        subscription.ref = item.ref;
        this.subscriptions.push(subscription);

      });

    }})
  }

  addSubscription(){

    this.db.collection<Subscription>('subscriptions').add(this.formSubscription.value).then((result)=>{
      this.alert.successAlert('Subscriptions', 'added correctly');
      this.formSubscription.reset();
    }).catch((error)=>{
      this.alert.errorAlert('Subscriptions', 'an error has occurred');
    } )

    this.showSubscriptions();

  }

  getSubscription(subscription: Subscription){

    this.isEdit = true;

    this.formSubscription.setValue({
      name: subscription.name,
      price: subscription.price,
      duration: subscription.duration,
      period: subscription.period,
    })

    this.id = subscription.id;

  }

  editSubscription(){

    this.db.doc('subscriptions/'+this.id).update(this.formSubscription.value).then((result)=>{
      this.alert.successAlert('Subscriptions', 'update correctly');
      this.formSubscription.reset();
      this.showSubscriptions();
    }).catch((error)=>{
      this.alert.errorAlert('Subscriptions', 'an error has occurred');
    } )

  }

}
