import { DocumentReference } from '@angular/fire/firestore';

export class Client{
  id: string;
  name: string;
  surname: string;
  email: string;
  birthdate: string;
  phone: number;
  profileImgUrl: string;
  nif: string;
  ref: DocumentReference;
  visible: boolean;

  constructor(){

  }
}
