import { DocumentReference } from '@angular/fire/firestore';

export class Subscription{
  id: string;
  name: string;
  price: number;
  duration: number;
  period: number;
  ref: DocumentReference
}
