import { DocumentReference } from '@angular/fire/firestore';

export class Inscription{
  date: Date;
  endPeriod: Date;
  price: DocumentReference;
  client: DocumentReference;
  subtotal: number;
  iva: number;
  total: number;

  constructor(){
    this.date = null;
    this.endPeriod = null;
    this.price = this.price;
    this.client = this.client;
    this.subtotal = this.subtotal;
    this.iva = this.iva;
    this.total = this.total;
  }


  validate(): any{

    let response = {
      valid: false,
      message: ''
    }


    if(this.client == null || this.client == undefined){
      response.valid = false;
      response.message = 'No client has been set';
      return response;
    }

    if(this.price == null || this.price == undefined){
      response.valid = false;
      response.message = 'No price has been set';
      return response;
    }

    if(this.subtotal == undefined){
      response.valid = false;
      response.message = 'the subtotal could not be calculated';
      return response;
    }

    if(this.total == undefined){
      response.valid = false;
      response.message = 'the total could not be calculated';
      return response;
    }

    if(this.date == null || this.date == undefined){
      response.valid = false;
      response.message = 'No start date has been set';
      return response;
    }

    if(this.endPeriod == null || this.endPeriod == undefined){
      response.valid = false;
      response.message = 'No end date has been set';
      return response;
    }

    response.valid = true;
    return response;

  }
}
