import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Client } from '../models/client';


@Component({
  selector: 'app-select-client',
  templateUrl: './select-client.component.html',
  styleUrls: ['./select-client.component.scss']
})
export class SelectClientComponent implements OnInit {

  clients: Client[] = new Array<Client>();

  @Input('name') name: string;

  @Output('clientSelect')  clientSelect = new EventEmitter();

  @Output('clientCancel')  clientCancel = new EventEmitter();


  constructor(private db: AngularFirestore) { }

  ngOnInit() {

    this.db.collection<any>('clients').get().subscribe((result)=>{

      this.clients.length = 0;

      result.docs.forEach(item => {

        let client: any  = item.data();
        client.id = item.id;
        client.ref = item.ref;
        client.visible = false;

        this.clients.push(client);
      });
    });

  }

  searchClient(name: string){

    this.clients.forEach((client) => {
      if(client.name.toLocaleLowerCase().includes(name.toLocaleLowerCase()) || client.nif.toLocaleLowerCase().includes(name.toLocaleLowerCase())  || client.surname.toLocaleLowerCase().includes(name.toLocaleLowerCase())){
        client.visible = true;
      }else{
        client.visible = false;
      }
    })

  }

  selectClient(client: Client){

    this.name = client.name + ' ' + client.surname;
    this.clients.forEach((client)=>{
      client.visible = false;
    })

    this.clientSelect.emit(client);

  }


  cancelClient(){

    this.name = undefined;

    this.clientCancel.emit();

  }
}
